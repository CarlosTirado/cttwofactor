﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Logic.Config
{
    public static class CTTwoFactorConfig
    {
        private static int DefaultExpiryMinutes = 30;
        public static int? ManualExpiryMinutes { get; set; }
        public static int GetExpiryMinutes()
        {
            if (ManualExpiryMinutes.HasValue) return ManualExpiryMinutes.Value;
            else return DefaultExpiryMinutes;
        }
    }
}
