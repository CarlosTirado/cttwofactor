﻿using CTTwoFactor.Logic.Base;
using CTTwoFactor.Logic.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Logic.Interactors
{
    public class ValidateCodeInteractor : TemplateInteractor
    {
        ValidateCodeInteractorRequest _request => (ValidateCodeInteractorRequest)request;
        ValidateCodeInteractorResponse _response => (ValidateCodeInteractorResponse)response;
        protected override bool Validations()
        {
            response = new ValidateCodeInteractorResponse();
            if (string.IsNullOrEmpty(_request.UserName))
            {
                _response.Error = true;
                _response.Message = "The UserName is Required";
                return false;
            }

            if (string.IsNullOrEmpty(_request.Code))
            {
                _response.Error = true;
                _response.Message = "The Code is Required";
                return false;
            }

            return true;
        }
        protected override void Process()
        {
            var codeFound = context.GeneratedCodes.FirstOrDefault(t =>
                t.UserName == _request.UserName &&
                t.Code == _request.Code &&
                t.Category == _request.Category &&
                t.ExpiryDate > DateTime.Now &&
                t.State == StateCodeEnumeration.Generated.Value);

            if(codeFound != null)
            {
                codeFound.State = StateCodeEnumeration.Validated.Value;
                codeFound.UpdateDate = DateTime.Now;

                _response.Error = false;
                _response.Message = "OK";
                _response.IsCorrect = true;
            }
            else
            {
                _response.Error = false;
                _response.Message = "OK";
                _response.IsCorrect = false;
            }
        }
    }
    public class ValidateCodeInteractorRequest: InteractorRequest
    {
        public string UserName { get; set; }
        public string Category { get; set; }
        public string Code { get; set; }
    }
    public class ValidateCodeInteractorResponse: InteractorResponse
    {
        public bool IsCorrect { get; set; }
    }
}
