﻿using CTTwoFactor.Data.Models;
using CTTwoFactor.Logic.Base;
using CTTwoFactor.Logic.Config;
using CTTwoFactor.Logic.Enumerations;
using CTTwoFactor.Logic.Services;
using System;
using System.Linq;

namespace CTTwoFactor.Logic.Interactors
{
    public class GenerateCodeInteractor : TemplateInteractor
    {
        GenerateCodeInteractorRequest _request => (GenerateCodeInteractorRequest)request;
        GenerateCodeInteractorResponse _response => (GenerateCodeInteractorResponse)response;
        protected override bool Validations()
        {
            response = new GenerateCodeInteractorResponse();

            var anteriorCodigoGeneradoGenerado = context.GeneratedCodes.FirstOrDefault(t =>
                t.UserName == _request.UserName &&
                t.Category == _request.Category &&
                t.State == StateCodeEnumeration.Generated.Value);

            if (anteriorCodigoGeneradoGenerado == null)
            {
                return true;
            }
            else
            {

                if (anteriorCodigoGeneradoGenerado.ExpiryDate >= DateTime.Now)
                {
                    _response.Error = true;
                    _response.Message = "The user has an active code to validate";
                    return false;

                }
                else
                {
                    anteriorCodigoGeneradoGenerado.State = StateCodeEnumeration.Lost.Value;
                    anteriorCodigoGeneradoGenerado.UpdateDate = DateTime.Now;
                    return true;
                }
            }
        }

        protected override void Process()
        {
            var generateCodeService = new GenerateCodeService(context);

            var generateCodeRequest = new GenerateCodeServiceRequest();
            generateCodeRequest.UserName = _request.UserName;
            generateCodeRequest.Category= _request.Category;

            var generateCodeResponse = generateCodeService.Generate(generateCodeRequest);
            context.GeneratedCodes.Add(generateCodeResponse.GeneratedCode);

            _response.Error = false;
            _response.Message = "OK";
            _response.UserName = _request.UserName;
            _response.GeneratedCode = generateCodeResponse.GeneratedCode.Code;
            _response.Category = generateCodeResponse.GeneratedCode.Category;
            _response.ExpiryDate = generateCodeResponse.GeneratedCode.ExpiryDate;
        }

        
    }
    public class GenerateCodeInteractorRequest : InteractorRequest 
    {
        public string UserName { get; set; }
        public string Category { get; set; }
    }
    public class GenerateCodeInteractorResponse: InteractorResponse
    {
        public string UserName { get; set; }
        public string Category { get; set; }
        public string GeneratedCode { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
