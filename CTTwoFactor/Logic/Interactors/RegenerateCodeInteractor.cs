﻿using CTTwoFactor.Data.Models;
using CTTwoFactor.Logic.Base;
using CTTwoFactor.Logic.Config;
using CTTwoFactor.Logic.Enumerations;
using CTTwoFactor.Logic.Services;
using System;
using System.Linq;

namespace CTTwoFactor.Logic.Interactors
{
    public class RegenerateCodeInteractor : TemplateInteractor
    {
        RegenerateCodeInteractorRequest _request => (RegenerateCodeInteractorRequest)request;
        RegenerateCodeInteractorResponse _response => (RegenerateCodeInteractorResponse)response;
        protected override bool Validations()
        {
            response = new RegenerateCodeInteractorResponse();

            var anteriorCodigoGeneradoActivo = context.GeneratedCodes.FirstOrDefault(t =>
                t.UserName == _request.UserName &&
                t.Category == _request.Category &&
                t.State == StateCodeEnumeration.Generated.Value);

            if (anteriorCodigoGeneradoActivo != null)
            {
                anteriorCodigoGeneradoActivo.State = StateCodeEnumeration.Lost.Value;
                anteriorCodigoGeneradoActivo.UpdateDate = DateTime.Now;
            }

            return true;
        }

        protected override void Process()
        {
            var RegenerateCodeService = new GenerateCodeService(context);

            var RegenerateCodeRequest = new GenerateCodeServiceRequest();
            RegenerateCodeRequest.UserName = _request.UserName;
            RegenerateCodeRequest.Category = _request.Category;

            var RegenerateCodeResponse = RegenerateCodeService.Generate(RegenerateCodeRequest);
            context.GeneratedCodes.Add(RegenerateCodeResponse.GeneratedCode);

            _response.Error = false;
            _response.Message = "OK";
            _response.UserName = _request.UserName;
            _response.GeneratedCode = RegenerateCodeResponse.GeneratedCode.Code;
            _response.ExpiryDate = RegenerateCodeResponse.GeneratedCode.ExpiryDate;
        }

        
    }
    public class RegenerateCodeInteractorRequest : InteractorRequest 
    {
        public string UserName { get; set; }
        public string Category { get; set; } 
    }
    public class RegenerateCodeInteractorResponse: InteractorResponse
    {
        public string UserName { get; set; }
        public string Category { get; set; }
        public string GeneratedCode { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
