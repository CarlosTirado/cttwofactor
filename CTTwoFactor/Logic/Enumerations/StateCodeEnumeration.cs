﻿using CTTwoFactor.Logic.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Logic.Enumerations
{
    public class StateCodeEnumeration : Enumeration<StateCodeEnumeration, string>
    {
        public static readonly StateCodeEnumeration Generated = new StateCodeEnumeration("GENERATED", "Generated");
        public static readonly StateCodeEnumeration Validated = new StateCodeEnumeration("VALIDATED", "Validated");
        public static readonly StateCodeEnumeration Lost = new StateCodeEnumeration("LOST", "Lost");


        private StateCodeEnumeration(string value, string displayName) : base(value, displayName) { }

        public static string GetDisplayName(string value)
        {
            return GetAll().FirstOrDefault(t => t.Value == value)?.DisplayName ?? value.ToString();
        }
    }
}