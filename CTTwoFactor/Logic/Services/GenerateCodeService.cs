﻿using CTTwoFactor.Data;
using CTTwoFactor.Data.Models;
using CTTwoFactor.Logic.Config;
using CTTwoFactor.Logic.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Logic.Services
{
    internal class GenerateCodeService
    {
        private readonly TwoFactorContext _context;
        internal GenerateCodeService(TwoFactorContext context)
        {
            _context = context;
        }

        internal GenerateCodeServiceResponse Generate(GenerateCodeServiceRequest _request)
        {
            var code = new GeneratedCode();
            code.UserName = _request.UserName;
            code.State = StateCodeEnumeration.Generated.Value;
            code.Category = _request.Category;

            while (string.IsNullOrEmpty(code.Code))
            {
                code.Code = new Random().Next(100000, 999999).ToString();

                var codeDuplicateNoExpited = _context.GeneratedCodes.FirstOrDefault(t =>
                    t.Code == code.Code &&
                    t.Category == code.Category &&
                    t.State == StateCodeEnumeration.Generated.Value &&
                    t.ExpiryDate > DateTime.Now);

                if (codeDuplicateNoExpited != null)
                {
                    code.Code = null;
                }
            }

            code.ExpiryDate = DateTime.Now.AddMinutes(CTTwoFactorConfig.GetExpiryMinutes());
            code.RegisterDate = DateTime.Now;
            code.UpdateDate = DateTime.Now;

            GenerateCodeServiceResponse response = new GenerateCodeServiceResponse();
            response.GeneratedCode = code;
            return response;
        }
    }
    internal class GenerateCodeServiceRequest
    {
        public string UserName { get; set; }
        public string Category { get; set; }
    }
    internal class GenerateCodeServiceResponse
    {
        public GeneratedCode GeneratedCode { get; set; }
    }
}
