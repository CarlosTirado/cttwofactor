﻿using CTTwoFactor.Logic.Interactors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor
{
    public class HandlerTwoFactor
    {
        public GenerateCodeInteractorResponse GenerateCode(GenerateCodeInteractorRequest request)
        {
            var interactor = new GenerateCodeInteractor();
            return (GenerateCodeInteractorResponse) interactor.Execute(request);
        }
        public RegenerateCodeInteractorResponse RegenerateCode(RegenerateCodeInteractorRequest request)
        {
            var interactor = new RegenerateCodeInteractor();
            return (RegenerateCodeInteractorResponse)interactor.Execute(request);
        }
        public ValidateCodeInteractorResponse ValidateCode(ValidateCodeInteractorRequest request)
        {
            var interactor = new ValidateCodeInteractor();
            return (ValidateCodeInteractorResponse)interactor.Execute(request);
        }
    }
}
