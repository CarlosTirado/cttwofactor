﻿using CTTwoFactor.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Data
{
    public class TwoFactorContext : DbContext
    {
        public TwoFactorContext() : base("TwoFactorContext") { }

        public DbSet<GeneratedCode> GeneratedCodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasDefaultSchema("CTTwoFactor");
        }
    }
} 