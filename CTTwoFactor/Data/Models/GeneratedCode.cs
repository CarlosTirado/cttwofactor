﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTTwoFactor.Data.Models
{
    public class GeneratedCode
    {
        [Key]
        public long Id { get; set; }

        [Index]
        [MaxLength(15)]
        public string UserName { get; set; }

        [Index]
        [MaxLength(30)]
        public string Category { get; set; }

        [Index]
        [MaxLength(6)]
        public string Code { get; set; }

        [Index]
        public DateTime ExpiryDate { get; set; }

        [Index]
        [MaxLength(20)]
        public string State { get; set; }

        [Index]
        public DateTime RegisterDate { get; set; }

        [Index]
        public DateTime UpdateDate { get; set; }
    }
}
