﻿using System;
using CTTwoFactor.Logic.Config;
using CTTwoFactor.Logic.Interactors;
using CTTwoFactor.Logic.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CTTwoFactor.Test
{
    [TestClass]
    public class TestCTTwoFactor
    {
        [TestMethod]
        public void TestGenerate()
        {
            CTTwoFactorConfig.ManualExpiryMinutes = 60;
            var handlerTwoFactor = new HandlerTwoFactor(); 

            var generateCodeInteractorRequest = new GenerateCodeInteractorRequest();
            generateCodeInteractorRequest.UserName = "1065649116";
            generateCodeInteractorRequest.Category = "PRUEBA";
            var generateCodeInteractorResponse = handlerTwoFactor.GenerateCode(generateCodeInteractorRequest);
            Assert.AreEqual(false, generateCodeInteractorResponse.Error);
            Assert.AreEqual(false, string.IsNullOrEmpty(generateCodeInteractorResponse.GeneratedCode));


            var regenerateCodeInteractorRequest = new RegenerateCodeInteractorRequest();
            regenerateCodeInteractorRequest.UserName = "1065649116";
            regenerateCodeInteractorRequest.Category = "PRUEBA";
            var regenerateCodeInteractorResponse = handlerTwoFactor.RegenerateCode(regenerateCodeInteractorRequest);
            Assert.AreEqual(false, regenerateCodeInteractorResponse.Error);
            Assert.AreEqual(false, string.IsNullOrEmpty(regenerateCodeInteractorResponse.GeneratedCode));


            var validateCodeInteractorRequest = new ValidateCodeInteractorRequest();
            validateCodeInteractorRequest.UserName = "1065649116";
            validateCodeInteractorRequest.Category = "PRUEBA";
            validateCodeInteractorRequest.Code = regenerateCodeInteractorResponse.GeneratedCode;
            var validateCodeInteractorResponse = handlerTwoFactor.ValidateCode(validateCodeInteractorRequest);
            Assert.AreEqual(false, validateCodeInteractorResponse.Error);
            Assert.AreEqual(true, validateCodeInteractorResponse.IsCorrect);

        }
    }
}
